package market;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import javax.swing.*;

import static java.lang.Integer.parseInt;

public class Main {

    private static final int    PORT = 5001;
    private static final String ADDRESS = "127.0.0.1";

    String          id;
    BufferedReader  in;
    PrintWriter     out;
    BrokerMessage   brokerMessage = new BrokerMessage();
    Market          market = new Market();

    JFrame      frame = new JFrame("Market");
    JTextField  textField = new JTextField(40);
    JTextArea   messageArea = new JTextArea(8, 40);

    public static void main(String[] args) throws Exception {
        Main client = new Main();
        client.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        client.frame.setVisible(true);
        client.run();
    }

    public Main() {

        textField.setEditable(true);
        messageArea.setEditable(true);
        frame.getContentPane().add(textField, "North");
        frame.getContentPane().add(new JScrollPane(messageArea), "Center");
        frame.pack();

        textField.addActionListener(e -> {
            out.println(textField.getText());
            textField.setText("");
        });
    }

    String addCheckSum(String s) {
        return s + "10=" + s.length();
    }

    private void run() throws IOException {
        Socket socket = new Socket(ADDRESS, PORT);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        out = new PrintWriter(socket.getOutputStream(), true);

        boolean requestSuccessful = false;

        while (true) {
            messageArea.setText(market.getMarket());
            String line = in.readLine();
            if (line.startsWith("SETID")) {
                this.id = line.substring(6);
            } else {
                try {
                    parseBrokerMessage(line);
                    switch (brokerMessage.command) {
                        case ("BUY"): {
                            requestSuccessful = market.buy(brokerMessage.item, parseInt(brokerMessage.amount));
                            System.out.println("BUY ORDER:" + requestSuccessful);
                            System.out.println(market.getMarket());
                            messageArea.setText(market.getMarket());
                            break;
                        }
                        case ("SELL"): {
                            requestSuccessful = market.sell(brokerMessage.item, parseInt(brokerMessage.amount));
                            System.out.println("SALE ORDER:" + requestSuccessful);
                            System.out.println(market.getMarket());
                            messageArea.setText(market.getMarket());
                            break;
                        }
                    }
                    String message = "49=" + brokerMessage.id + "|STATUS=" + requestSuccessful + "|";
                    out.println(addCheckSum(message));
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public static class Item {
        public String   item;
        public int   amount;

        Item(String item, int amount) {
            this.item    = item;
            this.amount  = amount;
        }
    }

    public static class Market {
        ArrayList<Item> items = new ArrayList<>();

        Market() {
            items.add(new Item("GOLD", 1000000));
            items.add(new Item("PLAT", 1000000));
            items.add(new Item("OIL", 1000000));
        }

        String getMarket() {
            String market = "";

            for (Item item: items) {
                market += item.item + " " + item.amount + "\n";
            }

            return market;
        }

        boolean buy(String item, int amount) {
            for (Item i : items) {
                if (i.item.equals(item)) {
                    if (i.amount >= amount) {
                        i.amount -= amount;
                        return true;
                    }
                }
            }
            return false;
        }

        boolean sell(String item, int amount) {
            for (Item i : items) {
                if (i.item.equals(item)) {
                    i.amount += amount;
                    return true;
                }
            }
            return false;
        }
    }

    public static class BrokerMessage {
        public String   id = null;
        public String   command = null;
        public String   item = null;
        public String   amount = null;
        public String   checksum = null;

        boolean isValid() {
            return ((id != null) && (command != null) && (item != null) && (amount != null) && (checksum != null));
        }

        void clear() {
            id = null;
            command = null;
            item = null;
            amount = null;
            checksum = null;
        }
    }

    private void parseBrokerMessage(String input) {
        String[] fields = input.split("\\|");

        for (String field : fields) {
            String[] value = field.split("=");
            if (value.length != 2) {
                throw new IllegalArgumentException("Invalid message\nArgument length");
            }
            parseBrokerValue(value);
        }
        if (!brokerMessage.isValid()) {
            throw new IllegalArgumentException("Invalid message\nNot enough information");
        }
        int length = input.length() - ("10=" + brokerMessage.checksum).length();
        if (length != parseInt(brokerMessage.checksum)) {
            throw new IllegalArgumentException("Invalid message\nMessage doesnt match checksum length\n" + " " + input.length() + " " + brokerMessage.checksum );
        }
    }

    private void parseBrokerValue(String[] value) {
        switch (value[0]) {
            case ("49"): {
                brokerMessage.id = value[1];
                break;
            }
            case ("COMMAND"): {
                brokerMessage.command = value[1];
                break;
            }
            case ("ITEM"): {
                brokerMessage.item = value[1];
                break;
            }
            case ("AMOUNT"): {
                brokerMessage.amount = value[1];
                break;
            }
            case ("10"): {
                brokerMessage.checksum = value[1];
                break;
            }
            default:
                throw new IllegalArgumentException("WHAT DID YOU ENTER!?!?\n" + value[0]);
        }
    }

}