package router;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static java.lang.Integer.parseInt;

public class Main {
    private static final int BROKER_PORT = 5000;
    private static final int MARKET_PORT = 5001;

    public static HashMap<String, PrintWriter> brokers = new HashMap<>();
    public static HashMap<String, PrintWriter> markets = new HashMap<>();

    public static void main(String[] args) {
        Router router = new Router();

        router.start();
    }

    private static class Router {

        public void start() {
            Executor executor = Executors.newFixedThreadPool(2);

            Runnable brokerController = () -> {
                try {
                    openBroker();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };
            Runnable marketController = () -> {
                try {
                    openMarket();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            };

            executor.execute(brokerController);
            executor.execute(marketController);
        }

        public void openBroker() throws Exception {
            Executor executor = Executors.newFixedThreadPool(2);

            System.out.println("The broker server is running.");
            ServerSocket listener = new ServerSocket(BROKER_PORT);
            try {
                while (true) {
                    Broker hand = new Broker(listener.accept());
                    Runnable run = hand::run;
                    executor.execute(run);
                }
            } finally {
                listener.close();
            }
        }

        public void openMarket() throws Exception {
            Executor executor = Executors.newFixedThreadPool(2);

            System.out.println("The market server is running.");
            ServerSocket listener = new ServerSocket(MARKET_PORT);
            try {
                while (true) {
                    Market market = new Market(listener.accept());
                    Runnable run = market::run;
                    executor.execute(run);
                }
            } finally {
                listener.close();
            }
        }

    }

    public static String addCheckSum(String s) {
        return s + "10=" + s.length();
    }

    private static class MarketMessage {
        String  broker_id = null;
        String  order_status = null;
        String  checksum = null;

        boolean isValid() {
            return ((broker_id != null) && (order_status != null) && (checksum != null));
        }

        void clear() {
            broker_id = null;
            order_status = null;
            checksum = null;
        }
    }

    private static class Market {
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private String id;
        MarketMessage marketMessage = new MarketMessage();

        public Market(Socket socket) {
            this.socket = socket;
        }

        String getANewID () {
            int r = new Random().nextInt(800000) + 100000;
            while (brokers.containsKey(String.valueOf(r))) {
                r = new Random().nextInt(800000) + 100000;
            }
            return String.valueOf(r);
        }

        public void run() {
            try {
                id = getANewID();
                in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);
                out.println("SETID " + id);

                markets.put(id, out);

                while (true) {
                    String input = in.readLine();
                    marketMessage.clear();
                    //im assuming its null when the client disconnects
                    if (input == null) {
                        return;
                    }
                    try {
                        parseMarketMessage(input);
                        System.out.println("VALID MARKET MESSAGE!!! ");
                        for (Map.Entry broker: brokers.entrySet()) {
                            System.out.println(broker.getKey() + " " + marketMessage.broker_id);
                            if (broker.getKey().equals(marketMessage.broker_id)) {
                                String message = "STATUS=" + marketMessage.order_status + "|";
                                ((PrintWriter)broker.getValue()).println(addCheckSum(message));
                                System.out.println("SENT MESSAGE TO BROKER " + marketMessage.broker_id);
                            }
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("INVALID MESSAGE!!!\n" + e.getMessage());
                    }
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                if (out != null) {
                    brokers.remove(id);
                }
                try {
                    socket.close();
                } catch (IOException ignored) {
                }
            }
        }

        private void parseMarketMessage(String input) {
            String[] fields = input.split("\\|");

            for (String field : fields) {
                String[] value = field.split("=");
                if (value.length != 2) {
                    throw new IllegalArgumentException("Invalid message\nArgument length");
                }
                parseMarketValue(value);
            }
            if (!marketMessage.isValid()) {
                throw new IllegalArgumentException("Invalid message\nNot enough information");
            }
            int length = input.length() - ("10=" + marketMessage.checksum).length();
            if (length != parseInt(marketMessage.checksum)) {
                throw new IllegalArgumentException("Invalid message\nMessage doesnt match checksum length\n" + "Mess: " + input.length() + " Check: " + marketMessage.checksum + " Act: " + length );
            }
        }

        private void parseMarketValue(String[] value) {
            switch (value[0]) {
                case ("49"): {
                    marketMessage.broker_id = value[1];
                    break;
                }
                case ("STATUS"): {
                    marketMessage.order_status = value[1];
                    break;
                }
                case ("10"): {
                    marketMessage.checksum = value[1];
                    break;
                }
                default:
                    throw new IllegalArgumentException("WHAT DID YOU ENTER!?!?\n" + value[0]);
            }
        }

    }



    public static class BrokerMessage {
        public String   id = null;
        public String   command = null;
        public String   item = null;
        public String   amount = null;
        public String   checksum = null;

        boolean isValid() {
            return ((id != null) && (command != null) && (item != null) && (amount != null) && (checksum != null));
        }

        void clear() {
            id = null;
            command = null;
            item = null;
            amount = null;
            checksum = null;
        }
    }

    private static class Broker {
        private Socket socket;
        private BufferedReader in;
        private PrintWriter out;
        private String id;
        private BrokerMessage brokerMessage = new BrokerMessage();

        public Broker(Socket socket) {
            this.socket = socket;
        }

        String getANewID() {
            int r = new Random().nextInt(800000) + 100000;
            while (brokers.containsKey(String.valueOf(r))) {
                r = new Random().nextInt(800000) + 100000;
            }
            return String.valueOf(r);
        }

        public void run() {
            try {
                id = getANewID();
                in = new BufferedReader(new InputStreamReader( socket.getInputStream()));
                out = new PrintWriter(socket.getOutputStream(), true);
                out.println("SETID " + id);

                brokers.put(id, out);

                while (true) {
                    String input = in.readLine();
                    brokerMessage.clear();
                    //im assuming its null when the client disconnects
                    if (input == null) {
                        return;
                    }
                    try {
                        parseBrokerMessage(input);
                        System.out.println("VALID BROKER MESSAGE!!! ");
                        for (Map.Entry market : markets.entrySet()) {
                            ((PrintWriter)market.getValue()).println(input);
                        }
                    } catch (IllegalArgumentException e) {
                        System.out.println("INVALID MESSAGE!!!\n" + e.getMessage());
                    }
                }
            } catch (IOException e) {
                System.out.println(e.getMessage());
                if (out != null) {
                     brokers.remove(id);
                }
                try {
                    socket.close();
                }
                catch (IOException ignored) {
                }
            }
        }

        private void parseBrokerMessage(String input) {
            String[] fields = input.split("\\|");

            for (String field : fields) {
                String[] value = field.split("=");
                if (value.length != 2) {
                    throw new IllegalArgumentException("Invalid message\nArgument length");
                }
                parseBrokerValue(value);
            }
            if (!brokerMessage.isValid()) {
                throw new IllegalArgumentException("Invalid message\nNot enough information");
            }
            int length = input.length() - ("10=" + brokerMessage.checksum).length();
            if (length != parseInt(brokerMessage.checksum)) {
                throw new IllegalArgumentException("Invalid message\nMessage doesnt match checksum length\n" + "Mess: " + input.length() + " Check: " + brokerMessage.checksum + " Act: " + length );
            }
        }

        private void parseBrokerValue(String[] value) {
            switch (value[0]) {
                case ("49"): {
                    brokerMessage.id = value[1];
                    break;
                }
                case ("COMMAND"): {
                    brokerMessage.command = value[1];
                    break;
                }
                case ("ITEM"): {
                    brokerMessage.item = value[1];
                    break;
                }
                case ("AMOUNT"): {
                    brokerMessage.amount = value[1];
                    break;
                }
                case ("10"): {
                    brokerMessage.checksum = value[1];
                    break;
                }
                default:
                    throw new IllegalArgumentException("WHAT DID YOU ENTER!?!?\n" + value[0]);
            }
        }
    }
}

//49=100110|COMMAND=BUY|ITEM=GOLD|AMOUNT=100|10=43

