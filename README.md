# FIX me

A School project made in Java
The main focus of this project was to understand how to use java to make apps over a network
where we would need to have a client, a router, and a server programmed and have them all compile with Maven
The project was also to improve our understanding of Maven

This network project had to be made to use some features of the FIX Protocol used in FOREX trading
You needed to created a router, (clients)broker and (server)market and you'd be able to buy and sell things from the market

It was the final school project for our Java module

# To run
Make sure to have Maven install and configured correctly on your machine
mvn install

Open router.jar through a terminal as it doesnt have a gui, but once its started up you can then open the Market.jar and Broker.jar
You can open multiple Brokers but you should only have one Market running

# Program Preview

![](https://gitlab.com/Heagan/fixme/raw/master/fix.PNG)


